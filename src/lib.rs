use std::sync::{Condvar, Mutex, MutexGuard, RwLock, RwLockReadGuard, RwLockWriteGuard, PoisonError};
use std::mem;

pub struct MVar<T> {
    value: RwLock<Option<T>>,
    mutex: Mutex<()>,
    condvar: Condvar,
}

#[must_use]
pub struct MVarReadGuard<'a, T: 'a> {
    inner: RwLockReadGuard<'a, Option<T>>,
}

impl<'a, T> ::std::ops::Deref for MVarReadGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &T {
        self.inner.deref().as_ref().unwrap()
    }
}

#[derive(Debug)]
pub enum MVarError<'a, T: 'a> {
    MutexPoisonError(PoisonError<MutexGuard<'a, ()>>),
    RwLockReadPoisonError(PoisonError<RwLockReadGuard<'a, Option<T>>>),
    RwLockWritePoisonError(PoisonError<RwLockWriteGuard<'a, Option<T>>>),
}

pub type MVarResult<'a, T, U> = Result<T, MVarError<'a, U>>;

impl<'a, T> ::std::convert::From<PoisonError<MutexGuard<'a, ()>>> for MVarError<'a, T> {
    fn from(e: PoisonError<MutexGuard<'a, ()>>) -> Self {
        MVarError::MutexPoisonError(e)
    }
}

impl<'a, T> ::std::convert::From<PoisonError<RwLockReadGuard<'a, Option<T>>>> for MVarError<'a, T> {
    fn from(e: PoisonError<RwLockReadGuard<'a, Option<T>>>) -> Self {
        MVarError::RwLockReadPoisonError(e)
    }
}

impl<'a, T> ::std::convert::From<PoisonError<RwLockWriteGuard<'a, Option<T>>>> for MVarError<'a, T> {
    fn from(e: PoisonError<RwLockWriteGuard<'a, Option<T>>>) -> Self {
        MVarError::RwLockWritePoisonError(e)
    }
}

impl<T> MVar<T> {
    pub fn new(value: Option<T>) -> Self {
        MVar {
            mutex: Mutex::new(()),
            condvar: Condvar::new(),
            value: RwLock::new(value),
        }
    }

    fn is_present(&self) -> MVarResult<bool, T> {
        Ok(try!(self.value.read()).is_some())
    }

    fn lock(&self) -> MVarResult<(MutexGuard<()>, bool), T> {
        let lock = try!(self.mutex.lock());
        let present = try!(self.is_present());
        Ok((lock, present))
    }

    fn wait_for_presence(&self, presence: bool) -> MVarResult<(MutexGuard<()>, bool), T> {
        let (mut lock, mut present) = try!(self.lock());
        while present != presence {
            lock = try!(self.condvar.wait(lock));
            present = try!(self.is_present());
        }
        Ok((lock, present))
    }

    pub fn take(&self) -> MVarResult<T, T> {
        let (_lock, _) = try!(self.wait_for_presence(true));
        let value = try!(self.value.write()).take().unwrap();
        self.condvar.notify_one();
        Ok(value)
    }

    pub fn try_take(&self) -> MVarResult<Option<T>, T> {
        let (_lock, present) = try!(self.lock());
        if !present { return Ok(None); }
        let value = try!(self.value.write()).take().unwrap();
        self.condvar.notify_one();
        Ok(Some(value))
    }

    pub fn put(&self, value: T) -> MVarResult<(), T> {
        let _lock = try!(self.wait_for_presence(false));
        *try!(self.value.write()) = Some(value);
        self.condvar.notify_all();
        Ok(())
    }

    pub fn try_put(&self, value: T) -> MVarResult<bool, T> {
        let (_lock, present) = try!(self.lock());
        if present { return Ok(false); }
        *try!(self.value.write()) = Some(value);
        self.condvar.notify_all();
        Ok(true)
    }

    pub fn read(&self) -> MVarResult<MVarReadGuard<T>, T> {
        {
            let value_guard = try!(self.value.read());
            if value_guard.is_some() {
                return Ok(MVarReadGuard { inner: value_guard });
            }
        }

        let _present = self.wait_for_presence(true);
        Ok(MVarReadGuard { inner: try!(self.value.read()) })
    }

    pub fn try_read(&self) -> MVarResult<RwLockReadGuard<Option<T>>, T> {
        Ok(try!(self.value.read()))
    }

    pub fn write(&self, value: T) -> MVarResult<(), T> {
        let _lock = try!(self.mutex.lock());
        *try!(self.value.write()) = Some(value);
        self.condvar.notify_all();
        Ok(())
    }

    pub fn modify<R, F: FnOnce(&mut T) -> R>(&self, f: F) -> MVarResult<R, T> {
        let _lock = try!(self.wait_for_presence(true));
        Ok(f(try!(self.value.write()).as_mut().unwrap()))
    }

    pub fn try_modify<R, F: FnOnce(&mut T) -> R>(&self, f: F) -> MVarResult<Option<R>, T> {
        Ok(if let Some(value) = try!(self.value.write()).as_mut() {
            Some(f(value))
        } else {
            None
        })
    }

    pub fn swap(&self, value: &mut T) -> MVarResult<(), T> {
        self.modify(|v| mem::swap(value, v))
    }

    pub fn try_swap(&self, value: &mut T) -> MVarResult<bool, T> {
        Ok(try!(self.try_modify(|v| mem::swap(value, v))).is_some())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::Arc;
    use std::thread;

    #[test]
    fn take_put() {
        let mv1 = Arc::new(MVar::new(None));
        let mv2 = mv1.clone();
        let t = thread::spawn(move || {
            mv2.put(3).unwrap();
        });
        assert_eq!(mv1.take().unwrap(), 3);
        t.join().unwrap();
    }

    #[test]
    fn write() {
        let mv = MVar::new(None);
        mv.write(1).unwrap();
        assert_eq!(*mv.read().unwrap(), 1);
        mv.write(2).unwrap();
        assert_eq!(*mv.read().unwrap(), 2);
    }

    #[test]
    fn swap() {
        let mv = MVar::new(Some(1));
        let mut x = 2;
        mv.swap(&mut x).unwrap();
        assert_eq!(*mv.read().unwrap(), 2);
    }

    #[test]
    fn trys() {
        let mv = MVar::new(None);
        assert_eq!(mv.try_take().unwrap(), None);
        assert_eq!(*mv.try_read().unwrap(), None);
        assert_eq!(mv.try_swap(&mut 0).unwrap(), false);
        assert_eq!(mv.try_put(1).unwrap(), true);
        assert_eq!(mv.try_put(2).unwrap(), false);
        assert_eq!(*mv.try_read().unwrap(), Some(1));
        assert_eq!(mv.try_swap(&mut 3).unwrap(), true);
        assert_eq!(mv.try_take().unwrap(), Some(3));
        assert_eq!(mv.try_take().unwrap(), None);
    }

    #[test]
    fn count() {
        let mv1 = Arc::new(MVar::new(None));
        let n = 4;
        let m = 2000;
        let mut ts = Vec::with_capacity(n);
        for _ in 0..n {
            let mv2 = mv1.clone();
            ts.push(thread::spawn(move || {
                for _ in 0..m {
                    let x = mv2.take().unwrap();
                    mv2.put(x + 1).unwrap();
                }
            }));
        }
        mv1.put(0).unwrap();
        for t in ts {
            t.join().unwrap();
        }
        assert_eq!(mv1.take().unwrap(), n * m);
    }

    #[test]
    fn read_many() {
        let mvx1 = Arc::new(MVar::new(None));
        let mvy1 = Arc::new(MVar::new(Some(0)));
        let n = 20;
        let mut ts = Vec::with_capacity(n);
        for _ in 0..n {
            let mvx2 = mvx1.clone();
            let mvy2 = mvy1.clone();
            ts.push(thread::spawn(move || {
                let x = *mvx2.read().unwrap();
                mvy2.modify(|y| *y += x).unwrap();
            }));
        }
        mvx1.put(1).unwrap();
        for t in ts {
            t.join().unwrap();
        }
        assert_eq!(mvy1.take().unwrap(), n);
    }
}
